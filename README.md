# mysq-express
Building REST APIs with MySQL and Node.js 

Installation:
=============

1-clone the project and install npm

  command: $sudo npm install 

2-create new  database in your local server and export  database.sql 
 
Configuration file  here :/config/config.js

module.exports = {

     hostname:'localhost',
     username:'root',
     password:'password',
     database:'database_name',
     user_email:'',
     user_password :''
}

3-start projetc :

 command: $cd /path_projetc

 command: $npm start

congratulation....!
